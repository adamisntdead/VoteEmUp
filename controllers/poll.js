var Poll = require("../models/Poll.js");

exports.newPoll = function(req, res) {
    var poll = new Poll();
    var optRaw = req.body.options.replace(/\r\n/g, "%20").split("%20");
    var opt = [];

    for (var i = 0; i < optRaw.length; i++) {
        opt.push([optRaw[i], 0]);
    }

    poll.creator = req.user.id;
    poll.title = req.body.title;
    poll.options = opt;

    poll.save(function(err) {
        if (err) {
            res.send(err);
        }
        console.log(poll);
        res.redirect('/');
    });

};

exports.getNew = function(req, res) {
    res.render('new', {
        title: 'New'
    });
};

exports.getUser = function(req, res) {
    Poll.find({
        'creator': req.params.user
    }, function(err, docs) {
        if (err) {
            res.send(err);
        }
        res.render('home', {
            title: 'My Polls',
            polls: docs
        });
    });
};

exports.getPoll = function(req, res) {
    Poll.findById(req.params.id, function(err, poll) {
        var byUser = false;
        if (err) {
            res.send(err);
        }
        var empty = true;
        for (var i = 0; i < poll.options.length; i++) {
            if (poll.options[i][1] > 0) {
                empty = false;
            }
        }
        var optionsWithoutValue = [];
        for (var i = 0; i < poll.options.length; i++) {
            optionsWithoutValue.push(poll.options[i][0]);
        }
        if (req.user != null) {
            if (poll.creator == req.user.id) {
                byUser = true;
            }
        }

        console.log(poll);
        var toReturn = {};
        toReturn.title = poll.title;
        toReturn.options = poll.options;
        toReturn.creator = poll.creator;
        toReturn.optionsWithoutValue = optionsWithoutValue;
        toReturn.byUser = byUser;
        toReturn.id = poll._id;
        res.render('poll', {
            title: toReturn.title,
            poll: toReturn,
            empty: empty
        });
    });
};

exports.newVote = function(req, res) {
    // Save the Poll
    function save(id, poll) {
        Poll.update({
            _id: id
        }, {
            ips: poll.ips,
            options: poll.options
        }, {
            upsert: true
        }, function(err) {
            if (err) throw err;
        });
    }

    // Find the correct poll
    Poll.findById(req.params.id, function(err, poll) {
        if (err) throw err;

        // Add IP, Save and Redirect
        function finish(ip) {
            poll.ips.push(ip);
            save(req.params.id, poll);
            res.redirect(/poll/ + req.params.id);
        }

        // Function to Check if Ip has voted - returnes false if not voted
        function ipHasVoted(ip) {
            for (var i = 0; i < poll.ips.length; i++) {
                if (ip == poll.ips[i]) {
                    return true;
                }
            }
            return false;
        }

        // If the user has not voted
        if (ipHasVoted(req.connection.remoteAddress) == false) {
            // Check if the user has picked the other option
            if (req.body.option == 'otherSecretCodeSoIDontMessUp') {
                poll.options.push([req.body.other, 1]);
                finish(req.connection.remoteAddress);
            }
            // If they don't want the other
            else {
                for (var i = 0; i < poll.options.length; i++) {
                    if (req.body.option == poll.options[i][0]) {
                        console.log('Found it');
                        poll.options[i][1]++;
                        finish(req.connection.remoteAddress);
                    }
                }
            }

        }
        // If the user has voted
        else {
            res.render('voted', {
                title: 'Already Voted!'
            });
        }
    });
};

exports.deletePoll = function(req, res) {
    Poll.findById(req.params.id, function(err, poll) {
        if (err) throw err;

        if (poll.creator == req.user.id) {
            Poll.remove({
                _id: req.params.id
            }, function(err) {
                if (err) console.log(err);
                res.redirect('/');
            });

        }
    });
};