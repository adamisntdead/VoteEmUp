var Poll = require('../models/Poll.js');
/**
 * GET /
 */
exports.index = function(req, res) {
  Poll.find({}, function(err, docs){
    if (err) {res.send(err);}
    res.render('home', {
      title: 'Home',
      polls: docs
    });
  });
};
