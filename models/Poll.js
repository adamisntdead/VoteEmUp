var mongoose = require('mongoose');

var schemaOptions = {
  timestamps: true,
  toJSON: {
    virtuals: true
  }
};

var pollSchema = mongoose.Schema({
    creator: String,
    title: String,
    options: [],
    ips: []
}, schemaOptions);



// Export It
var Poll = mongoose.model('Poll', pollSchema);
module.exports = Poll;